use anyhow::{bail, Result};
use clap_verbosity_flag::Verbosity;
use log::debug;
use reqwest::Url;
use structopt::StructOpt;

mod client;
mod entities;
mod issues;
mod merge_requests;

use client::Client;
use entities::*;
use merge_requests::*;

// Command line options
#[derive(Debug, StructOpt)]
#[structopt(rename_all = "kebab-case")]
struct Cli {
    #[structopt(long, short = "s", env = "GITLAB_CLERK_API_SERVER")]
    /// Host to be contacted (defaults to host from CI_API_V4_URL)
    api_server: Url,
    // -------------------------------------------------------------------------
    #[structopt(
        long,
        short = "t",
        env = "GITLAB_CLERK_PRIVATE_TOKEN",
        hide_env_values = true
    )]
    /// User private token
    private_token: String,
    // -------------------------------------------------------------------------
    #[structopt(subcommand)]
    cmd: Command,
    // Quick and easy logging setup
    #[structopt(flatten)]
    verbosity: Verbosity,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
enum Command {
    /// Manage merge requests
    Merges(MergeSubcommand),
    /// Manage issues
    Issues(IssueSubcommand),
    /// Manage milestones
    Milestones(MilestoneSubcommand),
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
struct MergeSubcommand {
    #[structopt(subcommand)]
    cmd: Merges,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
struct IssueSubcommand {
    #[structopt(subcommand)]
    cmd: Issues,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
struct MilestoneSubcommand {
    #[structopt(subcommand)]
    cmd: Milestones,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
enum Merges {
    /// Rebase and merge ready MRs
    MergeReady(MergeReady),
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
enum Issues {
    #[structopt(name = "single")]
    /// Single issue
    SingleIssue(SingleIssue),
    // -------------------------------------------------------------------------
    #[structopt(name = "list")]
    /// List issues
    ListIssues(ListIssues),
    // -------------------------------------------------------------------------
    #[structopt(name = "new")]
    /// New issue
    NewIssue(NewIssue),
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
enum Milestones {
    #[structopt(name = "list")]
    /// List milestones
    ListMilestones(ListMilestones),
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
struct MergeReady {
    #[structopt(
        long,
        short = "l",
        env = "GITLAB_CLERK_MERGE_LABELS",
        default_value = "🤖::Merge+ready+🐙"
    )]
    /// Labels to search for ready to merge MRs, comma separated
    labels: String,
    // -------------------------------------------------------------------------
    #[structopt(
        long,
        short = "e",
        env = "GITLAB_CLERK_MERGE_EMOJI",
        default_value = "octopus"
    )]
    /// Emoji to search for ready to merge MRs
    emoji: String,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
struct SingleIssue {
    #[structopt(long, short = "p", env = "GITLAB_CLERK_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    project: String,
    /// Issue id
    issue_iid: isize,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
struct ListIssues {
    #[structopt(long, short = "s", default_value = "opened")]
    /// Filter issues by state: all, opened, closed
    state: IssueState,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "p", env = "GITLAB_CLERK_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    project: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "l")]
    /// Filter issues by comma-separated list of label names, issues must have all labels to be returned.
    /// `None` lists all issues with no labels. `Any` lists all issues with at least one label.
    /// `No+Label` (Deprecated) lists all issues with no labels. Predefined names are case-insensitive.
    labels: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "m")]
    /// Filter issues by the milestone title. `None` lists all issues with no milestone.
    /// `Any` lists all issues that have an assigned milestone.
    milestone: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "x")]
    /// Execute command for each returned issue (in a bash subshell)
    ///
    /// The current issue is exported as environment variables:
    /// `issue_id`, `issue_title`, `issue_description` (optional)
    ///
    /// Example:
    ///
    /// `... issues list -x 'echo ${issue_id}: ${issue_title}'`
    execute: Option<String>,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
struct NewIssue {
    #[structopt(long, short = "p", env = "GITLAB_CLERK_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    project: String,
    // -------------------------------------------------------------------------
    /// Issue title
    title: String,
    // -------------------------------------------------------------------------
    /// Issue description
    #[structopt(long, short = "d")]
    description: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "l")]
    /// Comma-separated list of label names
    labels: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "m")]
    /// Milestone name
    milestone: Option<String>,
}

impl From<NewIssue> for CreateIssue {
    /// Create a new `CreateIssue` struct.
    /// NOTE: This struct has no milestone_id because it must be resolved from the name.
    fn from(issue: NewIssue) -> CreateIssue {
        CreateIssue {
            project_id: issue.project,
            title: issue.title,
            description: issue.description,
            labels: issue.labels,
            ..Default::default()
        }
    }
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
struct ListMilestones {
    #[structopt(long, short = "p", env = "GITLAB_CLERK_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    project: String,
}

fn import_env() {
    dotenv::dotenv().ok();
    dotenv::from_filename(".private.env").ok();
    let _import_env = |source, dest| {
        if let Some(api_server) = std::env::var_os(source) {
            if std::env::var_os(dest) == None {
                std::env::set_var(dest, &api_server);
            }
        }
    };
    _import_env("CI_API_V4_URL", "GITLAB_CLERK_API_SERVER");
    _import_env("CI_PROJECT_PATH", "GITLAB_CLERK_PROJECT");
}
fn setup_env_logger(verbosity: &Verbosity) -> Result<()> {
    use log::Level;
    let level_filter = verbosity
        .log_level()
        .unwrap_or(Level::Warn)
        .to_level_filter();
    env_logger::Builder::new()
        .filter(Some("gitlab_clerk"), level_filter)
        .filter(None, Level::Warn.to_level_filter())
        .try_init()?;
    Ok(())
}
fn main() -> Result<()> {
    import_env();
    let args = Cli::from_args();
    setup_env_logger(&args.verbosity)?;

    let client = Client::new(args.api_server, &args.private_token)?;

    let print_issue = |issue: Issue| {
        println!("* Issue #{}: {}", issue.id, issue.title);
        if let Some(description) = issue.description {
            println!("  {}", description);
        }
    };

    fn print_milestones(milestones: Vec<Milestone>) {
        println!("Found {} milestones:", milestones.len());
        for milestone in milestones {
            println!("- {}", milestone.title);
        }
    }

    match args.cmd {
        Command::Merges(subcommand) => match subcommand.cmd {
            Merges::MergeReady(merges) => mergebot(&client, &merges.labels, &merges.emoji),
        },
        Command::Issues(subcommand) => match subcommand.cmd {
            Issues::NewIssue(new_issue) => {
                let milestone = match new_issue.milestone.as_ref() {
                    None => None,
                    Some(milestone_title) => {
                        let milestone = Milestone::in_project_by_title(
                            &client,
                            &new_issue.project,
                            milestone_title,
                        )?;
                        if milestone.is_none() {
                            let milestones = Milestone::in_project(&client, &new_issue.project)?;
                            print_milestones(milestones);
                            bail!("Ambiguous milestone");
                        }
                        milestone
                    }
                };
                let mut create_issue: CreateIssue = new_issue.into();
                create_issue.milestone_id = milestone.map(|m| m.id);
                debug!("Creating new_issue: {:?}", create_issue);
                Issue::new(&client, &create_issue)?;
                Ok(())
            }
            Issues::SingleIssue(single_issue) => {
                let issue = Issue::single(&client, &single_issue.project, single_issue.issue_iid)?;
                print_issue(issue);
                Ok(())
            }
            Issues::ListIssues(issues) => {
                let issue_list = Issue::list(
                    &client,
                    &issues.project,
                    &issues.state,
                    &issues.labels,
                    &issues.milestone,
                )?;

                fn execute(shell_command: &str, issue: &Issue) {
                    use std::process::Command;
                    let mut issue_command = Command::new("bash");
                    issue_command
                        .arg("-c")
                        .arg(&shell_command)
                        .env("issue_id", format!("{}", issue.id))
                        .env("issue_title", format!("{}", issue.title));
                    if let Some(description) = &issue.description {
                        issue_command.env("issue_description", description);
                    }
                    issue_command
                        .spawn()
                        .expect(&format!("Error executing: {}", &shell_command));
                }

                if let Some(ref shell_command) = issues.execute {
                    for issue in issue_list {
                        execute(shell_command, &issue);
                    }
                } else {
                    println!("---------------------------------");
                    for issue in issue_list {
                        print_issue(issue);
                    }
                    println!("---------------------------------");
                }
                Ok(())
            }
        },
        Command::Milestones(subcommand) => match subcommand.cmd {
            Milestones::ListMilestones(milestone) => {
                let milestones = Milestone::in_project(&client, &milestone.project)?;
                print_milestones(milestones);
                Ok(())
            }
        },
    }
}
