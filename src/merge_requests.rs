//! ## Manage ready to merge GitLab MRs
//!
//! If in your project you prefer a semi-linear history, and you have a queue of MRs that are ready to be
//! merged, you may and up fighting for the merge and rebasing more than once each branch (the problem is more
//! evident if the project has a lengthy pipeline to wait).
//!
//! This bot automate a workflow that emerged in our team:
//!
//! 1. Developer A work on some feature
//! 2. Once done, assign the MR to developer B for the review
//! 3. Instead of merging (or rebasing) developer B adds a ~"Merge Ready" label
//! 4. The TL manages the merge queue (doing a second review on the code)
//!
//! Waiting for this bot to exist, we ended up finding the second review valuable, so we modified the bot to
//! wait for both the ~"Merge Ready" label and the TL emoji of choice.
//!
//! The new workflow is very like the one above, with a different ending:
//!
//! 1. Developer A work on some feature
//! 2. Once done, assign the MR to developer B for the review
//! 3. Instead of merging (or rebasing) developer B adds a ~"Merge Ready" label
//! 4. The TL does a second review on the code, and adds an `:octopus:` emoji
//! 5. `gitlab-clerk` loops and picks the first rebased MR or triggers a rebase

use std::collections::HashSet;

use anyhow::Result;
use log::{debug, info};
use reqwest::Url;

use crate::client::Client;
use crate::entities::*;

impl MergeRequest {
    pub fn list_ready_to_merge(
        client: &reqwest::Client,
        api_server: &Url,
        merge_labels: &str,
        merge_emoji: &str,
    ) -> Result<Vec<MergeRequest>> {
        let api_path = format!(
            "api/v4/merge_requests?state=opened&scope=assigned_to_me&order_by=created_at&labels={}&my_reaction_emoji={}",
            merge_labels,
            merge_emoji
        );
        let full_url = api_server.join(&api_path)?;
        debug!("Ready to query {:?}", full_url);
        let mut merge_requests: Vec<MergeRequest> =
            client.get(full_url).send()?.error_for_status()?.json()?;
        // TODO: We should reassign back all WIP or conflicting MRs
        merge_requests.retain(|mr| {
            !mr.work_in_progress
                && mr.merge_status == "can_be_merged"
                && mr.target_branch == "master"
        });
        Ok(merge_requests)
    }
    pub fn infos(&self, client: &reqwest::Client, api_server: &Url) -> Result<MergeRequestDetails> {
        let mr_uri = format!(
            "api/v4/projects/{}/merge_requests/{}",
            self.project_id, self.iid
        );
        let info_mr_uri = api_server.join(&mr_uri)?;
        let message = GetMergeRequestDetails::from(self);
        let resp = client.get(info_mr_uri).json(&message).send()?.json()?;
        Ok(resp)
    }
    fn auto_remove_branch(
        &self,
        client: &reqwest::Client,
        api_server: &Url,
    ) -> Result<MergeRequest> {
        let mr_uri = format!(
            "api/v4/projects/{}/merge_requests/{}",
            self.project_id, self.iid
        );
        let update_mr_url = api_server.join(&mr_uri)?;
        let force_merge = RemoveSourceBranch::from(self);
        debug!("  Update url: {:?} <- {:?}", update_mr_url, force_merge);
        // We may set the remove after merge option in the merge command too
        let resp = client
            .put(update_mr_url)
            .json(&force_merge)
            .send()?
            .json()?;
        Ok(resp)
    }
    fn pipelines(&self, client: &reqwest::Client, api_server: &Url) -> Result<Vec<Pipeline>> {
        let pipelines_uri = format!(
            "api/v4/projects/{}/merge_requests/{}/pipelines",
            self.project_id, self.iid
        );
        let pipelines_mr_url = api_server.join(&pipelines_uri)?;
        let message = ListPipelines::from(self);
        debug!("querying pipelines: {:?} {:?}", pipelines_mr_url, message);
        let resp = client.get(pipelines_mr_url).json(&message).send()?.json()?;
        Ok(resp)
    }
    fn accept(&self, client: &reqwest::Client, api_server: &Url) -> Result<MergeRequest> {
        let accept_uri = format!(
            "api/v4/projects/{}/merge_requests/{}/merge",
            self.project_id, self.iid
        );
        let accept_mr_uri = api_server.join(&accept_uri)?;
        let message = AcceptMergeRequest::from(self);
        let mut resp = client.put(accept_mr_uri).json(&message).send()?;
        debug!("accept response: {:?}", resp);
        let resp = resp.json()?;
        Ok(resp)
    }
    fn rebase(&self, client: &reqwest::Client, api_server: &Url) -> Result<()> {
        let rebase_uri = format!(
            "api/v4/projects/{}/merge_requests/{}/rebase",
            self.project_id, self.iid
        );
        let rebase_mr_url = api_server.join(&rebase_uri)?;
        let message = RebaseBranch::from(self);
        let _resp = client.put(rebase_mr_url).json(&message).send()?;
        Ok(())
    }
    fn emojis(&self, client: &reqwest::Client, api_server: &Url) -> Result<Vec<Emoji>> {
        let emoji_uri = format!(
            "api/v4/projects/{}/merge_requests/{}/award_emoji",
            self.project_id, self.iid
        );
        let emoji_mr_uri = api_server.join(&emoji_uri)?;
        let message = AwardEmoji::from(self);
        debug!("querying emojis: {:?} {:?}", emoji_mr_uri, message);
        let resp = client.get(emoji_mr_uri).json(&message).send()?.json()?;
        Ok(resp)
    }
    fn create(
        client: &reqwest::Client,
        api_server: &Url,
        mr: CreateMergeRequest,
    ) -> Result<MergeRequest> {
        //! POST /projects/:id/merge_requests
        let create_mr = format!("api/v4/projects/{}/merge_requests", mr.project_id);
        let create_mr_uri = api_server.join(&create_mr)?;
        debug!("creating mr: {:?} {:?}", create_mr_uri, mr);
        let resp = client.post(create_mr_uri).json(&mr).send()?.json()?;
        Ok(resp)
    }
}

#[derive(Debug)]
pub enum MergeResult {
    RebaseInProgress(MergeRequestDetails),
    Merged(MergeRequest),
    Rebasing(MergeRequest),
    NothingToDo,
}

/// Try to merge the given MR, optionally rebasing and waiting for pipeline completition.
pub fn try_to_merge(
    client: &reqwest::Client,
    api_server: &Url,
    mr: MergeRequest,
) -> Result<MergeResult> {
    let details = mr.infos(client, api_server)?;
    debug!("Trying to merge {:?}", details);
    // The first rebase_in_progress is a signal this is not the right time to do anything
    if Some(true) == details.rebase_in_progress {
        info!("Rebasing MR {:?}, need to wait", details);
        return Ok(MergeResult::RebaseInProgress(details));
    }
    if let Some(pipeline) = details.pipeline {
        // The first successful pipeline is the first candidate to be accepted
        if pipeline.status == "success" {
            mr.auto_remove_branch(client, api_server)?;
            // We may need to rebase, but we have no info about it from the API
            // so we try to accept
            debug!("----- Try to accept");
            if let Ok(amr) = mr.accept(client, api_server) {
                info!("Going to merge MR {:?}", amr);
                // After the merge all the queue will need to be rebased
                return Ok(MergeResult::Merged(amr));
            } else {
                // We may need to rebase
                debug!("----- About to rebase");
                mr.rebase(client, api_server)?;
                return Ok(MergeResult::Rebasing(mr));
            }
        }
    }
    return Ok(MergeResult::NothingToDo);
}

pub fn mergebot(client: &Client, merge_labels: &str, merge_emoji: &str) -> Result<()> {
    let resp = MergeRequest::list_ready_to_merge(
        &client.client,
        &client.api_server,
        &merge_labels,
        &merge_emoji,
    )?;
    // List all the MRs (assigned to the TL) with the requested label, those are the candidates to be merged.
    // Candidates MRs are those than can be merged without conflicts. The TL is supposed to manually review
    // them and then to add a marker emoji. Once an MR has both markers, the label from the reviewer and the
    // emoji from the TL, we can rebase the branch, push and accept the MR if the pipeline passes. Till there
    // is an accepted MR with a running pipeline, we need to wait the branch to be merged in the master to
    // rebase the next one.
    if resp.is_empty() {
        info!("No MR marked for merge");
        return Ok(());
    }
    let mut already_rebased = HashSet::new();
    for mr in resp.iter() {
        let details = mr.infos(&client.client, &client.api_server)?;
        if let Some(pipeline) = details.pipeline {
            if pipeline.status == "pending" || pipeline.status == "running" {
                info!("Some pipeline is running, not a good time to do anything");
                return Ok(());
            }
        }
        // ???: is missing like diverged_commits_count = 0 ?
        if details.diverged_commits_count.unwrap_or(0) == 0 {
            already_rebased.insert(details.iid);
        }
    }
    for mr in resp.into_iter().rev() {
        // We prefer any already rebased MR in the list
        // TODO: allow some priority label to merge hot-fixes
        if already_rebased.is_empty() || already_rebased.contains(&mr.iid) {
            let result = try_to_merge(&client.client, &client.api_server, mr)?;
            match result {
                MergeResult::NothingToDo => {
                    debug!("Check next MR...");
                }
                _ => {
                    info!("Some action ongoing, exiting {:?}", result);
                    break;
                }
            }
        }
    }
    Ok(())
}
