use serde::{Deserialize, Serialize};

#[derive(Deserialize, Debug)]
pub struct User {
    pub id: isize,
    pub username: String,
}

#[derive(Deserialize, Debug)]
pub struct Pipeline {
    pub id: isize,
    pub sha: String,
    #[serde(rename = "ref")]
    pub branch: String,
    pub status: String,
}

#[derive(Deserialize, Debug)]
pub struct Emoji {
    pub id: isize,
    pub name: String,
    pub user: User,
    pub awardable_id: isize,
    pub awardable_type: String,
}

/// Merge request message from GitLab
///
/// See [api/merge_requests](https://docs.gitlab.com/ee/api/merge_requests.html)
#[derive(Deserialize, Debug)]
pub struct MergeRequest {
    pub title: String,
    pub project_id: isize,
    pub iid: isize,
    pub work_in_progress: bool,
    pub author: User,
    pub merge_status: String,
    pub source_branch: String,
    pub target_branch: String,
    pub sha: String,
}

#[derive(Serialize, Debug)]
pub struct CreateMergeRequest {
    #[serde(rename = "id")]
    pub project_id: isize,
    pub title: String,
    pub source_branch: String,
    pub target_branch: String,
    pub description: Option<String>,
    pub assignee_ids: Vec<isize>,
    /// Comma separated
    pub labels: String,
}

#[derive(Deserialize, Debug)]
pub struct MergeRequestDetails {
    pub title: String,
    pub project_id: isize,
    pub iid: isize,
    pub work_in_progress: bool,
    pub author: User,
    pub merge_status: String,
    pub source_branch: String,
    pub target_branch: String,
    pub sha: String,
    // should be there with `include_rebase_in_progress=true`, but sometime is missing
    pub rebase_in_progress: Option<bool>,
    pub merge_error: Option<String>,
    pub pipeline: Option<Pipeline>,
    // should be there with `include_diverged_commits_count=true`, but perhaps 0 is not shown?
    pub diverged_commits_count: Option<i32>,
}

#[derive(Deserialize, Debug)]
pub struct Milestone {
    pub title: String,
    pub id: isize,
    pub iid: isize,
    pub project_id: isize,
    pub description: String,
    pub state: String,
}

#[derive(Serialize, Debug)]
pub struct CreateMilestone {
    #[serde(rename = "id")]
    pub project_id: isize,
    pub title: String,
    pub description: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct Issue {
    pub title: String,
    pub id: isize,
    pub iid: isize,
    pub state: IssueState,
    pub description: Option<String>,
    pub author: User,
    pub milestone: Option<Milestone>,
    pub project_id: isize,
    pub assignees: Vec<User>,
    pub weight: Option<isize>,
    pub labels: Vec<String>,
}

#[derive(Serialize, Debug, Default)]
pub struct CreateIssue {
    #[serde(rename = "id")]
    pub project_id: String,
    pub title: String,
    pub description: Option<String>,
    pub milestone_id: Option<isize>,
    pub assignee_ids: Vec<isize>,
    pub weight: Option<isize>,
    /// Comma separated
    pub labels: Option<String>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
pub enum IssueState {
    All,
    Opened,
    Closed,
}

impl IssueState {
    pub fn name(&self) -> &str {
        match self {
            IssueState::All => "all",
            IssueState::Closed => "closed",
            IssueState::Opened => "opened",
        }
    }
}

impl std::str::FromStr for IssueState {
    type Err = String;
    fn from_str(state: &str) -> Result<Self, Self::Err> {
        match state {
            "all" => Ok(IssueState::All),
            "closed" => Ok(IssueState::Closed),
            "opened" => Ok(IssueState::Opened),
            other_state => Err(format!(
                "Invalid state, {} not in [all, opened, closed]",
                other_state
            )
            .to_string()),
        }
    }
}

#[derive(Serialize, Debug)]
pub struct RebaseBranch {
    pub id: isize,
    pub merge_request_iid: isize,
}

impl<'a> From<&'a MergeRequest> for RebaseBranch {
    fn from(mr: &'a MergeRequest) -> RebaseBranch {
        RebaseBranch {
            id: mr.project_id,
            merge_request_iid: mr.iid,
        }
    }
}

#[derive(Serialize, Debug)]
pub struct RemoveSourceBranch {
    pub id: isize,
    pub merge_request_iid: isize,
    pub remove_source_branch: bool,
}

impl<'a> From<&'a MergeRequest> for RemoveSourceBranch {
    fn from(mr: &'a MergeRequest) -> RemoveSourceBranch {
        RemoveSourceBranch {
            id: mr.project_id,
            merge_request_iid: mr.iid,
            remove_source_branch: true,
        }
    }
}

#[derive(Serialize, Debug)]
pub struct ListPipelines {
    pub id: isize,
    pub merge_request_iid: isize,
}

impl<'a> From<&'a MergeRequest> for ListPipelines {
    fn from(mr: &'a MergeRequest) -> ListPipelines {
        ListPipelines {
            id: mr.project_id,
            merge_request_iid: mr.iid,
        }
    }
}

#[derive(Serialize, Debug)]
pub struct AcceptMergeRequest {
    pub id: isize,
    pub merge_request_iid: isize,
    pub merge_when_pipeline_succeeds: bool,
    pub should_remove_source_branch: bool,
    pub sha: String,
}

impl<'a> From<&'a MergeRequest> for AcceptMergeRequest {
    fn from(mr: &'a MergeRequest) -> AcceptMergeRequest {
        AcceptMergeRequest {
            id: mr.project_id,
            merge_request_iid: mr.iid,
            merge_when_pipeline_succeeds: true,
            should_remove_source_branch: true,
            sha: mr.sha.clone(),
        }
    }
}

#[derive(Serialize, Debug)]
pub struct GetMergeRequestDetails {
    pub id: isize,
    pub merge_request_iid: isize,
    pub include_diverged_commits_count: bool,
    pub include_rebase_in_progress: bool,
}

impl<'a> From<&'a MergeRequest> for GetMergeRequestDetails {
    fn from(mr: &'a MergeRequest) -> GetMergeRequestDetails {
        GetMergeRequestDetails {
            id: mr.project_id,
            merge_request_iid: mr.iid,
            include_diverged_commits_count: true,
            include_rebase_in_progress: true,
        }
    }
}

#[derive(Serialize, Debug)]
pub struct AwardEmoji {
    pub id: isize,
    pub awardable_id: isize,
}

impl<'a> From<&'a MergeRequest> for AwardEmoji {
    fn from(mr: &'a MergeRequest) -> AwardEmoji {
        AwardEmoji {
            id: mr.project_id,
            awardable_id: mr.iid,
        }
    }
}
