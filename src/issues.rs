use url::percent_encoding::{utf8_percent_encode, PATH_SEGMENT_ENCODE_SET};

use anyhow::Result;
use log::debug;

use crate::client::Client;
use crate::entities::{CreateIssue, Issue, IssueState, Milestone};

impl Issue {
    pub fn new(client: &Client, issue: &CreateIssue) -> Result<Issue> {
        //! `POST /projects/:id/issues`
        let api_path = {
            let project_name_or_id =
                utf8_percent_encode(&issue.project_id, PATH_SEGMENT_ENCODE_SET).to_string();
            format!("api/v4/projects/{}/issues", project_name_or_id)
        };
        let response = client.post_json(&api_path, &issue)?;
        let de = &mut serde_json::Deserializer::from_reader(response);
        let result: Result<Issue, _> = serde_path_to_error::deserialize(de);
        match result {
            Ok(issue) => Ok(issue),
            Err(err) => {
                dbg!(&api_path);
                dbg!(err.path().to_string());
                dbg!(client.get(&api_path)?.text()?);
                return Err(err.into_inner().into());
            }
        }
    }
    pub fn single(client: &Client, project: &str, issue_iid: isize) -> Result<Issue> {
        //! `GET /projects/:id/issues/:issue_iid`
        let api_path = {
            let project_name_or_id =
                utf8_percent_encode(project, PATH_SEGMENT_ENCODE_SET).to_string();
            format!(
                "api/v4/projects/{}/issues/{}",
                project_name_or_id, issue_iid
            )
        };
        let response = client.get(&api_path)?;
        let de = &mut serde_json::Deserializer::from_reader(response);
        let result: Result<Issue, _> = serde_path_to_error::deserialize(de);
        match result {
            Ok(issue) => Ok(issue),
            Err(err) => {
                dbg!(&api_path);
                dbg!(err.path().to_string());
                dbg!(client.get(&api_path)?.text()?);
                return Err(err.into_inner().into());
            }
        }
    }
    pub fn list(
        client: &Client,
        project: &Option<String>,
        state: &IssueState,
        labels: &Option<String>,
        milestone: &Option<String>,
    ) -> Result<Vec<Issue>> {
        let prefix = if let Some(aproject) = project {
            let project_name_or_id =
                utf8_percent_encode(aproject, PATH_SEGMENT_ENCODE_SET).to_string();
            format!("api/v4/projects/{}", project_name_or_id)
        } else {
            "api/v4".to_string()
        };
        let mut api_path = format!("{}/issues?state={}&scope=all", prefix, state.name());
        if let Some(labels) = labels {
            api_path = format!("{}&labels={}", api_path, labels);
        }
        if let Some(milestone) = milestone {
            api_path = format!("{}&milestone={}", api_path, milestone);
        }
        debug!("Ready to query {:?}", api_path);
        let mut all_issues: Vec<Issue> = vec![];
        for issues in client.get_paginated::<&str>(&api_path) {
            let issues = issues?;
            let de = &mut serde_json::Deserializer::from_reader(issues);
            let result: Result<Vec<Issue>, _> = serde_path_to_error::deserialize(de);
            let issues = match result {
                Ok(issues) => issues,
                Err(err) => {
                    dbg!(&api_path);
                    dbg!(err.path().to_string());
                    dbg!(client.get(&api_path)?.text()?);
                    return Err(err.into_inner().into());
                }
            };
            all_issues.extend(issues);
        }
        Ok(all_issues)
    }
}

impl Milestone {
    pub fn in_project_by_title(
        client: &Client,
        project: &str,
        title: &str,
    ) -> Result<Option<Milestone>> {
        // The API returns only a single item or nothing.
        Milestone::_in_project_by_title(client, project, Some(title)).map(|mut m| m.pop())
    }
    pub fn in_project(client: &Client, project: &str) -> Result<Vec<Milestone>> {
        // The API returns only a single item or nothing.
        Milestone::_in_project_by_title(client, project, None)
    }
    fn _in_project_by_title(
        client: &Client,
        project: &str,
        title: Option<&str>,
    ) -> Result<Vec<Milestone>> {
        let prefix = {
            let project_name_or_id =
                utf8_percent_encode(project, PATH_SEGMENT_ENCODE_SET).to_string();
            format!("api/v4/projects/{}", project_name_or_id)
        };
        let mut api_path = format!("{}/milestones?state=active", prefix);
        if let Some(title) = title {
            api_path = format!("{}&title={}", api_path, title);
        }
        debug!("Ready to query {:?}", api_path);
        let response = client.get(&api_path)?;
        let de = &mut serde_json::Deserializer::from_reader(response);
        let result: Result<Vec<Milestone>, _> = serde_path_to_error::deserialize(de);
        let milestones = match result {
            Ok(milestones) => milestones,
            Err(err) => {
                dbg!(&api_path);
                dbg!(err.path().to_string());
                dbg!(client.get(&api_path)?.text()?);
                return Err(err.into_inner().into());
            }
        };
        Ok(milestones)
    }
}
