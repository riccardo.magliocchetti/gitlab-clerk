_list:
	@just --list

bootstrap:
	cargo install cargo-bump

build:
	cargo build --release

deploy: build
	cp target/release/gitlab-clerk docker/

docker-build: deploy
	docker build --tag gitlab-clerk-test docker/

docker-test:
	just docker-run

docker-run ARGS="--help":
	docker run --rm -it gitlab-clerk-test /run/gitlab-clerk {{ARGS}}

docker-build-test: docker-build docker-test

bump-version PART="minor":
	cargo bump {{PART}}
