Gitlab Clerk
============

What is gitlab-clerk
--------------------

![the Grinch waiting GIF](https://media.giphy.com/media/ZcnwOpPTw9Ucw/giphy-downsized.gif)

`gitlab-clerk` is a GitLab cli, that wraps a growing number of GitLab APIs.

Most options can be given as command line arguments or through and environment variable.

`.env` is supported, a special `.private.env` is also loaded to allow private uncommited entries
beside the common ones:

- `.env` may contain `GITLAB_CLERK_PROJECT` and `GITLAB_CLERK_API_SERVER` (committed)
- `.private.env` may contain `GITLAB_CLERK_PRIVATE_TOKEN` (uncommitted / toplevel)

You can olso mix `dotenv` and [`direnv`](https://direnv.net/) as you like.

```raw
gitlab-clerk 0.7.0
Matteo Bertini <matteo@naufraghi.net>

USAGE:
    gitlab-clerk [FLAGS] --api-server <api-server> --private-token <private-token> <SUBCOMMAND>

FLAGS:
    -h, --help
            Prints help information

    -V, --version
            Prints version information

    -v, --verbosity
            Pass many times for more log output

            By default, it'll only report errors. Passing `-v` one time also prints warnings, `-vv` enables info
            logging, `-vvv` debug, and `-vvvv` trace.

OPTIONS:
    -s, --api-server <api-server>
            Host to be contacted (defaults to host from CI_API_V4_URL) [env:
            GITLAB_CLERK_API_SERVER=https://gitlab.com/]
    -t, --private-token <private-token>
            User private token [env: GITLAB_CLERK_PRIVATE_TOKEN]


SUBCOMMANDS:
    help      Prints this message or the help of the given subcommand(s)
    issues    Manage issues
    merges    Manage merge requests
```

### Subcommad `issues`

```raw
gitlab-clerk-issues 0.7.0
Matteo Bertini <matteo@naufraghi.net>
Manage issues

USAGE:
    gitlab-clerk --api-server <api-server> --private-token <private-token> issues <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    help    Prints this message or the help of the given subcommand(s)
    list    List issues
```

```raw
gitlab-clerk-issues-list 0.7.0
Matteo Bertini <matteo@naufraghi.net>
List issues

USAGE:
    gitlab-clerk issues list [OPTIONS]

FLAGS:
    -h, --help
            Prints help information

    -V, --version
            Prints version information


OPTIONS:
    -x, --execute <execute>
            Execute command for each returned issue (in a bash subshell)

            The current issue is exported as environment variables: `issue_id`, `issue_title`, `issue_description`
            (optional)

            Example:

            `... issues list -x 'echo ${issue_id}: ${issue_title}'`
    -l, --labels <labels>
            Filter issues by comma-separated list of label names, issues must have all labels to be returned. `None`
            lists all issues with no labels. `Any` lists all issues with at least one label. `No+Label` (Deprecated)
            lists all issues with no labels. Predefined names are case-insensitive.
    -m, --milestone <milestone>
            Filter issues by the milestone title. `None` lists all issues with no milestone. `Any` lists all issues that
            have an assigned milestone.
    -p, --project <project>
            Project name or id (defaults to CI_PROJECT_PATH) [env: GITLAB_CLERK_PROJECT=]

    -s, --state <state>
            Filter issues by state: all, opened, closed [default: opened]
```

### Subcommand `merges`

```raw
gitlab-clerk-merges 0.7.0
Matteo Bertini <matteo@naufraghi.net>
Manage merge requests

USAGE:
    gitlab-clerk --api-server <api-server> --private-token <private-token> merges <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    help           Prints this message or the help of the given subcommand(s)
    merge-ready    Rebase and merge ready MRs
```

```raw
gitlab-clerk-merges-merge-ready 0.7.0
Matteo Bertini <matteo@naufraghi.net>
Rebase and merge ready MRs

USAGE:
    gitlab-clerk merges merge-ready [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -e, --emoji <emoji>      Emoji to search for ready to merge MRs [env: GITLAB_CLERK_MERGE_EMOJI=]  [default: octopus]
    -l, --labels <labels>    Labels to search for ready to merge MRs, comma separated [env: GITLAB_CLERK_MERGE_LABELS=]
                             [default: Merge+ready+🐙]
```

This subcommand tries to merge a marked MR in the queue, rebasing and waiting for the pipeline to complete successfully.

Very like [Marge-bot](https://smarketshq.com/marge-bot-for-gitlab-keeps-master-always-green-6070e9d248df) but made to run inside the CI itself (in fact, `gitlab-clerk` is still in alpha and may eat your repos).

TL;DR: you have a project, you'd like an _always green_ master and a _semi linear_ history, so if your team is big enough you'll end up fighting to be the first to rebase and merge. An initial solution can be to make someone the _merger_, and the TL can in fact do this, having a second look[^1] on the code before, but the rebase/merge work is tedious.

Here enters `gitlab-clerk`:

1. Alice works on a feature and creates MR1, and assign it to Bob
2. Bob does the review and eventually adds the ~"Merge ready 🐙" label to the MR, and assigns the MR to the Tech Lead,
3. The TL does the final review and eventually adds the :octopus: emoji
4. `gitlab-clerk` triggers a rebase and/or merge actions

Usage
-----

You can run `gitlab-clerk` in a `while` loop or directly in the CI itself (you'll need to export some variables, and set up a schedule), like:

```yaml
gitlab-clerk:
  image: registry.gitlab.com/naufraghi/gitlab-clerk:latest
  variables:
    GIT_STRATEGY: none
  script:
    - /run/gitlab-clerk merges merge-ready
  only:
    - schedules
    - master
```

The current implementation does not use the _Approved_ feature, but relies only
on a label / emojii convention because I started using it before unlocking the
feature.

Notes
-----

[^1]: [Modern Code Review: A Case Study at Google](https://sback.it/publications/icse2018seip.pdf), 2 reviews seems a good number:
      ![image](/uploads/81406eba257a0ceb1105b9c5454b7235/image.png)
